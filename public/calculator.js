function  priceChange() {
    let a = quantity.value;
    let s = document.getElementsByName("Type");
    let select = s[0];
    let price = 0;
    let prices = getPrice();
    let priceIndex = prices.Types[select.value - 1];
    if (priceIndex >= 0 && !isNaN(priceIndex)) {
        price = prices.Types[priceIndex]*a;
    }
  
    let PenRadioDiv = document.getElementById("radiosPen");
    PenRadioDiv.style.display = (select.value == "pen" ? "block" : "none");
  
    let radiosPen = document.getElementsByName("PenOptions");
    radiosPen.forEach(function(radio) {
        if (radio.checked) {
            let optionPrice = prices.PenOptions[radio.value];
            if (optionPrice !== undefined) {
                price = price+ optionPrice*a;
            }
        }
    });

    let checkDiv = document.getElementById("checkboxPencil");
    checkDiv.style.display = (select.value == "pencil" ? "block" : "none");

    let checkboxPencil = document.querySelectorAll("#checkboxPencil input");
    checkboxPencil.forEach(function(checkbox) {
        if (checkbox.checked) {
            let propPrice = prices.PencilProperties[checkbox.name];
            if (propPrice !== undefined) {
                price =price+ propPrice*a;
            }
        }
    });
  
    let Product = document.getElementById("Product");
    Product.innerHTML = price + " рублей";
}

function getPrice() {
    return {
        Types: [80, 60, 20],
        
        PenOptions: {
            black: 10,
            red: 20,
        },
        PencilProperties: {
            eraser: 10,
            mechanical: 40,
        }
    };
}




window.addEventListener('DOMContentLoaded', function (event) {
    let PenRadioDiv = document.getElementById("radiosPen");
    PenRadioDiv.style.display = "none";
  
    let s = document.getElementsByName("Type");
    let select = s[0];

    select.addEventListener("change", function(event) {
        let target = event.target;
        console.log(target.value);
         priceChange();
    });
   
    let radiosPen = document.getElementsByName("PenOptions");
    radiosPen.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            let r = event.target;
            console.log(r.value);
             priceChange();
        });
    });
  
    let checkboxPencil = document.querySelectorAll("#checkboxPencil input");
    checkboxPencil.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            let c = event.target;
            console.log(c.name);
            console.log(c.value);
             priceChange();
        });
    });

     priceChange();
});